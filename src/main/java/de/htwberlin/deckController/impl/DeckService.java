package de.htwberlin.deckController.impl;

import java.util.ArrayList;
import java.util.Collections;


import de.htwberlin.deckController.domain.Card;
import de.htwberlin.deckController.domain.Deck;
import de.htwberlin.deckController.domain.Rank;
import de.htwberlin.deckController.domain.Suit;
import de.htwberlin.deckController.inter.DeckServiceInt;

public class DeckService implements DeckServiceInt{

	@Override
	public Deck generateDeck(Deck emptyDeck, int rules) {
		Suit[] suit = Suit.values();
		Rank[] rank = Rank.values();
		int count = rules;
		do {
			for(Suit s: suit) {
				for(Rank r: rank) {
					Card c = new Card(s,r);
					emptyDeck.getCards().add(c);
				}
			}
			count--;
		}while(count>0);
		return emptyDeck;
	}

	@Override
	public void mixCards(Deck deck) {
		Collections.shuffle(deck.getCards());	
	}
	
	@Override
	public boolean alertEmptyDeck(Deck deck) {
		return deck.getCards().isEmpty();	
	}

	@Override
	public void turnAroundDeck(Deck deck, Deck openDeck) {
		mixCards(openDeck);
		for(Card c: openDeck.getCards()) {
			deck.getCards().add(c);
		}
		openDeck.getCards().clear();		
	}

	@Override
	public Card turnFirstCard(Deck deck, Deck openDeck) {
		// TODO Auto-generated method stub
		Card openCard = deck.getCards().remove(0);
		openDeck.getCards().add(openCard);
		return openCard;
		
	}

	
	@Override
	public int countScore(Deck deck) {
		deck.getCards().trimToSize();
		int sum = 0;
		for(Card c : deck.getCards()) {
			sum+=getRankAsInt(c);
		}
		return sum;
	}

	@Override
	public int countCards(Deck deck) {
		return deck.getCards().size();
	}
	
	@Override
	public int getSuitAsInt(Card card) {
		
		if(card.getSuit()==Suit.hearts) 
			return 1;
		else if(card.getSuit()==Suit.diamonds)
			return 2;
		else if(card.getSuit()==Suit.clubs) 
			return 3;
		else  return 4;
	}
	
	@Override
	public int getRankAsInt(Card card) {
		if(card.getRank() == Rank.six) 
			return 6;
		else if(card.getRank() == Rank.seven)
			return 7;
		else if(card.getRank() == Rank.eight)
			return 7;
		else if(card.getRank() == Rank.nine) 
			return 9;
		else if(card.getRank() == Rank.ten)
			return 10;
		else if(card.getRank() == Rank.jack)
			return 11;
		else if(card.getRank() == Rank.queen)
			return 12;
		else if(card.getRank() == Rank.king)
			return 13;
		else  return 14;
	}
	
	@Override
	public Suit getSuitFromInt(int number) {
		if(number == 1) 
			return Suit.hearts;
		if(number == 2) 
			return Suit.diamonds;
		if(number == 3) 
			return Suit.clubs;
		if(number == 4) 
			return Suit.spades;
		else 
			return null;
	}
	
	@Override
	public Rank getRankFromInt(int number) {
		if(number == 6) 
			return Rank.six;
		if(number == 7) 
			return Rank.seven;
		if(number == 8) 
			return Rank.eight;
		if(number == 9) 
			return Rank.nine;
		if(number == 10) 
			return Rank.ten;
		if(number == 11) 
			return Rank.jack;
		if(number == 12) 
			return Rank.queen;
		if(number == 13) 
			return Rank.king;
		if(number == 14) 
			return Rank.ace;
		else return null;
	}

	
	@Override
	public ArrayList<String> getSuitsOfDeck(Deck deck) {
		ArrayList<String> suits = new ArrayList<String>();
		for(Card c: deck.getCards()) {
			suits.add(c.getSuit().toString());
		}
		return suits;
	}

	@Override
	public ArrayList<String> getRanksOfDeck(Deck deck) {
		ArrayList<String> ranks = new ArrayList<String>();
		for(Card c: deck.getCards()) {
			ranks.add(c.getRank().toString());
		}
		return ranks;
	}

}
