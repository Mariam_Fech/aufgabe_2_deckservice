package de.htwberlin.deckController.inter;

import java.util.ArrayList;

import de.htwberlin.deckController.domain.Card;
import de.htwberlin.deckController.domain.Deck;
import de.htwberlin.deckController.domain.Rank;
import de.htwberlin.deckController.domain.Suit;


public interface DeckServiceInt {
	
	/**
	 * Fills an empty deck in order of suites and ranks.
	 */
	public Deck generateDeck(Deck emptyDeck, int rules) ;	

	/**
	 * Shuffles the cards in deck.
	 *
	 * @param deck to be mixed
	 */
	public void mixCards(Deck deck);
	
	/**
	 * Alert empty deck.
	 *
	 * @param deck which is covered
	 * @return the boolean
	 */
	public boolean alertEmptyDeck(Deck deck);
	
	/**
	 * Turn the deck around and leave the last open card in the open deck.
	 *
	 * @param deck which is covered
	 * @param openDeck the deck for the played cards
	 */
	public void turnAroundDeck(Deck deck, Deck openDeck);
	
	/**
	 * Turn the deck around and leave the last open card in the open deck.
	 *
	 * @param deck which is covered
	 * @param openDeck the deck for the played cards
	 */
	public Card turnFirstCard(Deck deck, Deck openDeck);

	/**
	 * returns a cards suit as integer value
	 * @param card
	 * @return int value of suit: 1=hearts, 2=diamonds, 3=clubs, 4=spades
	 */
	public int getSuitAsInt(Card card);

	/**
	 * Returns a cards rank as integer value
	 * @param card
	 * @return inzt value of rank [6,14] accordingly [six,ace]
	 */
	int getRankAsInt(Card card);

	/**
	 * Returns the suit according to given int value/number
	 * @param number
	 * @return Suit accordingly: 1=hearts, 2=diamonds, 3=clubs, 4=spades
	 */
	public Suit getSuitFromInt(int number);

	/**
	 * Returns Rank according to privided int value/number
	 * @param number
	 * @return Rank value [6,14] accordingly [six,ace]
	 */
	public Rank getRankFromInt(int number);

	/**
	 * Counts the sum of values of cards in a deck
	 * @param deck
	 * @return int score
	 */
	public int countScore(Deck deck);

	/**
	 * Ccounts and returns the number of cards in deck.
	 * @return the count cards
	 */
	public int countCards(Deck deck);

	/**
	 * Returns a List with Suits of the Deck
	 * @param deck
	 * @return ArrayList<Suit> suits
	 */
	public ArrayList<String> getSuitsOfDeck(Deck deck);
	
	/**
	 * Returns a List with Ranks of the Deck
	 * @param deck
	 * @return ArrayList<Suit> ranks
	 */
	public ArrayList<String> getRanksOfDeck(Deck deck);
	
	
	
// Wird alles vom rule Controller übernommen	
//	/**
//	 * Check for fitting card in players deck.
//	 *
//	 * @param p the player
//	 * @param openCard the top card
//	 * @return the card which is to put on the open deck
//	 */
//	public Card checkMyCards(Player p, Card openCard);
//	
//	/**
//	 * Check for fitting card in case of wish.
//	 *
//	 * @param p the player
//	 * @param suit the suit to check for
//	 * @return the card which is to put on the open deck
//	 */
//	public Card checkMyCards(Player p, Suit suit);
	
}
