package de.htwberlin.deckController.domain;

public enum Rank {
	six, seven, eight, nine, ten, jack, queen, king, ace
}
