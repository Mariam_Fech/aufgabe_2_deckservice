package de.htwberlin.deckController.domain;

import java.util.ArrayList;


public class Deck {
	/** The cards. */
	private ArrayList<Card> cards;
	
	/**
	 * Instantiates a new deck.
	 */
	public Deck() {
		cards = new ArrayList<Card>();
	}
	
	/**
	 * Gets the cards.
	 *
	 * @return the cards
	 */
	public ArrayList<Card> getCards() {
		return cards;
	}

	
}
